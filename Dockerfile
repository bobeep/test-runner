FROM node:16-alpine

ENV NODE_ENV=production

WORKDIR /app

COPY package.json package-lock.json /app/
RUN npm ci --production

# Create app directory
ADD . /app

# Install app dependencies

CMD ["node", "index.js" ]